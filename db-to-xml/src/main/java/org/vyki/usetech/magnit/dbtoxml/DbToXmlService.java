package org.vyki.usetech.magnit.dbtoxml;

import org.vyki.usetech.magnit.dbtoxml.db.EntriesPgClient;
import org.vyki.usetech.magnit.dbtoxml.entity.Entries;
import org.vyki.usetech.magnit.dbtoxml.xml.EntriesXmlClient;

public class DbToXmlService {

    private int n;
    private String dbUsername;
    private String dbPassword;
    private String dbHost;
    private int dbPort;
    private String dbName;

    public void start() throws Exception {
        if (n > 300_000) System.out.println(n + " entries will be generated. It may take some time..");
        Entries selectedEntries = insertAndSelect();

        EntriesXmlClient entriesXmlClient = EntriesXmlClient.getInstance();
        entriesXmlClient.saveEntriesToFile(selectedEntries, "1.xml");
        entriesXmlClient.convertEntriesXmlWithXsl("1.xml", "2.xml");
        entriesXmlClient.summarizeEntryValuesFromXmlFile("2.xml");

    }

    private Entries insertAndSelect() throws Exception {
        try (EntriesPgClient pgClient = new EntriesPgClient(dbUsername, dbPassword, dbHost, dbPort, dbName)) {
            pgClient.truncateEntries();
            pgClient.insertEntries(n);
            return pgClient.selectAllEntries(n);
        }
    }


    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbHost() {
        return dbHost;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public int getDbPort() {
        return dbPort;
    }

    public void setDbPort(int dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
