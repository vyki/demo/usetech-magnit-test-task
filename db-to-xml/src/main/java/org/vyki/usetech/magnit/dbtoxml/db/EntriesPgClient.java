package org.vyki.usetech.magnit.dbtoxml.db;

import org.vyki.usetech.magnit.dbtoxml.entity.Entries;
import org.vyki.usetech.magnit.dbtoxml.entity.Entry;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntriesPgClient implements AutoCloseable {

    private static final int FETCH_SIZE = 100_000;
    private static final String TRUNCATE_QUERY = "truncate \"TEST\"";
    //    private static final String UPSERT_QUERY = "INSERT INTO \"TEST\"(field) " + // can be used instead of truncate+insert
    //            "VALUES (?) ON CONFLICT (field) DO UPDATE SET field=excluded.field";
    private static final String INSERT_QUERY = "INSERT INTO \"TEST\"(field) VALUES (?)";
    private static final String SELECT_QUERY = "SELECT * FROM \"TEST\" order by field limit ?";

    private Connection connection;

    public EntriesPgClient(String dbUsername, String dbPassword, String dbHost, int dbPort, String dbName) {

        try {
            connection = DriverManager.getConnection(buildDbEndpoint(dbHost, dbPort, dbName), dbUsername, dbPassword);
            connection.setAutoCommit(false);
        } catch (Exception e) {
            throw new RuntimeException("Exception occurred during connecting to Postgres: " + e.getMessage(), e);
        }

    }

    /* @TASK 2. После запуска, приложение вставляет в TEST N записей со значениями 1..N. Если в таблице TEST находились записи, то они удаляются перед вставкой. (1)*/
    public void truncateEntries() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.execute(TRUNCATE_QUERY);
            connection.commit();
        }
    }

    /* @TASK 2. После запуска, приложение вставляет в TEST N записей со значениями 1..N. Если в таблице TEST находились записи, то они удаляются перед вставкой. (2)*/
    public void insertEntries(int nMax) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_QUERY)) {
            statement.setFetchSize(FETCH_SIZE);
            for (int n = 1; n <= nMax; n++) {
                statement.setInt(1, n);
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
        System.out.println(nMax + " rows inserted into Postgres");
    }

    private String buildDbEndpoint(String host, int port, String dbName) {
        return "jdbc:postgresql://"
                + host
                + ":" + port
                + "/" + dbName;
    }

    /* @TASK 3. Затем приложение запрашивает эти данные из TEST.FIELD и формирует корректный XML-документ (1) */
    public Entries selectAllEntries(int n) throws SQLException {
        Entries entries = new Entries();
        List<Entry> entryList = new ArrayList<>(n);

        try (PreparedStatement statement = connection.prepareStatement(SELECT_QUERY)) {
            statement.setInt(1, n);
            statement.setFetchSize(FETCH_SIZE);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    entryList.add(new Entry(
                            resultSet.getInt(1)));
                }
            }
        }
        entries.setEntries(entryList);
        System.out.println("selected Entries size : " + entryList.size());
        return entries;
    }

    @Override
    public void close() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }
}
