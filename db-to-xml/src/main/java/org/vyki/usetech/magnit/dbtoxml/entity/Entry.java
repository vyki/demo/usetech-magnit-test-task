package org.vyki.usetech.magnit.dbtoxml.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Entry {
    private Integer field;

    public Entry() {
    }

    public Entry(Integer field) {
        this.field = field;
    }

    @XmlElement(name="field")
    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}
