package org.vyki.usetech.magnit.dbtoxml.xml;

import org.vyki.usetech.magnit.dbtoxml.entity.Entries;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class EntriesXmlClient {

    private EntriesXmlClient() {
    }

    private static class HOLDER {
        public static final EntriesXmlClient instance = new EntriesXmlClient();
    }

    public static EntriesXmlClient getInstance() {
        return HOLDER.instance;
    }

    /* @TASK 3. Затем приложение запрашивает эти данные из TEST.FIELD и формирует корректный XML-документ (2) */
    public void saveEntriesToFile(Entries entries, String fileName) throws JAXBException, IOException {
        JAXBContext jaxbEntries = JAXBContext.newInstance(Entries.class);
        Marshaller marshaller = jaxbEntries.createMarshaller();
        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        try (OutputStream os = new FileOutputStream(fileName)) {
            marshaller.marshal(entries, os);
        }
        System.out.println("All entries saved to " + fileName);
    }

    /* @TASK 4. Посредством XSLT, приложение преобразует содержимое "1.xml" к следующему виду */
    public void convertEntriesXmlWithXsl(String fileNameFrom, String fileNameTo) throws Exception {

        TransformerFactory factory = TransformerFactory.newInstance();
        try (InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("transform.xslt")) {
            Source xslt = new StreamSource(resourceAsStream);
            Transformer transformer = factory.newTransformer(xslt);

            Source fromXml = new StreamSource(new File(fileNameFrom));
            transformer.transform(fromXml, new StreamResult(new File(fileNameTo)));
        }
        System.out.println(fileNameFrom + " converted with transform.xslt and saved to " + fileNameTo);
    }

    /* @TASK 5. Приложение парсит "2.xml" и выводит арифметическую сумму значений всех атрибутов field в консоль. */
    public void summarizeEntryValuesFromXmlFile(String fileName) throws Exception {
        File xmlFile = new File(fileName);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);
        NodeList nodeList = document.getElementsByTagName("entry");

        long sum = 0;
        for (int i = 0; i < nodeList.getLength(); i++) {
            sum += Integer.parseInt(nodeList.item(i).getAttributes().item(0).getNodeValue());
        }
        System.out.println("Read and summarize all elements from " + fileName + " : " + sum);
    }

}
