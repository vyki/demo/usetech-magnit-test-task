
CREATE TABLE public."TEST"
(
    field numeric,
    PRIMARY KEY (field)
);

ALTER TABLE public."TEST"
    OWNER to postgres;