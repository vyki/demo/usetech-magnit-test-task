package org.vyki.usetech.magnit.runner;


import org.vyki.usetech.magnit.dbtoxml.DbToXmlService;

import java.util.*;

public class Main {

    private static final String N = "n";
    private static final String DB_USERNAME = "dbusername";
    private static final String DB_PASSWORD = "dbpassword";
    private static final String DB_HOST = "dbhost";
    private static final String DB_PORT = "dbport";
    private static final String DB_NAME = "dbname";
    private static final List<String> argumentNames = Arrays.asList(N, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME);
    private static final String EQUAL_SIGN = "=";

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        System.out.println("Start!\n");

        checkArgsNotEmpty(args);
        Map<String, String> keyValueArgs = extractKeyValueByEqualSign(args);
        checkAllArgsArePresent(keyValueArgs);

        DbToXmlService dbToXmlService = buildDbToXmlService(keyValueArgs);
        dbToXmlService.start();

        /* @TASK 6. При больших N (~1000000) время работы приложения не должно быть более пяти минут. */
        System.out.println("\nEnd! Total time ms: " + (System.currentTimeMillis() - startTime));
    }

    private static void checkArgsNotEmpty(String[] args) {
        if (args == null || args.length == 0 || args.length < argumentNames.size()) {
            throw new RuntimeException("All arguments are required: " + argumentNames + "\n" +
                    "example: n=100 dbUsername=postgres dbPassword=postgres dbHost=localhost dbPort=5432 dbName=mypostgres");
        }
    }

    private static Map<String, String> extractKeyValueByEqualSign(String[] args) {
        Map<String, String> argsMap = new HashMap<>();
        for (String arg : args) {
            arg = arg.toLowerCase();
            if (arg.equals(EQUAL_SIGN)) {
                throw new RuntimeException("Argument cannot be equal to \"=\"");
            } else if (!arg.contains(EQUAL_SIGN)) {
                throw new RuntimeException("Argument " + arg + " do not contain \"=\" as delimiter");
            }

            String[] keyValue = arg.split(EQUAL_SIGN);
            if (keyValue.length == 1) {
                throw new RuntimeException("Argument " + arg + " do not contain key or value");
            } else if (keyValue.length > 2) {
                throw new RuntimeException("Argument " + arg + " contain multiple \"=\" delimiters");
            }
            argsMap.put(keyValue[0], keyValue[1]);
        }
        return argsMap;
    }

    private static void checkAllArgsArePresent(Map<String, String> keyValueArgs) {
        for (String argName : argumentNames) {
            if (!keyValueArgs.containsKey(argName)) {
                throw new RuntimeException("Argument " + argName + " not specified");
            }
        }
        if (!isNumeric(keyValueArgs.get(N))) {
            throw new RuntimeException("\"n\" must be a positive integer");
        }
        if (!isNumeric(keyValueArgs.get(DB_PORT))) {
            throw new RuntimeException("\"dbport\" must be a positive integer");
        }
    }

    /* @TASK 1. Основной класс приложения должен следовать правилам JavaBean, то есть инициализироваться через setter'ы. Параметры инициализации - данные для подключения к БД и число N.  */
    private static DbToXmlService buildDbToXmlService(Map<String, String> keyValueArgs) {
        DbToXmlService dbToXmlService = new DbToXmlService();
        dbToXmlService.setN(Integer.parseInt(keyValueArgs.get(N)));
        dbToXmlService.setDbPort(Integer.parseInt(keyValueArgs.get(DB_PORT)));
        dbToXmlService.setDbUsername(keyValueArgs.get(DB_USERNAME));
        dbToXmlService.setDbPassword(keyValueArgs.get(DB_PASSWORD));
        dbToXmlService.setDbHost(keyValueArgs.get(DB_HOST));
        dbToXmlService.setDbName(keyValueArgs.get(DB_NAME));
        return dbToXmlService;
    }

    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
}
